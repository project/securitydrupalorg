<?php

/**
 * @file
 * sdo_permissions.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function sdo_permissions_user_default_roles() {
  $roles = array();

  // Exported role: administrator.
  $roles['administrator'] = array(
    'name' => 'administrator',
    'weight' => 0,
  );

  // Exported role: confirmed.
  $roles['confirmed'] = array(
    'name' => 'confirmed',
    'weight' => 9,
  );

  // Exported role: existing user.
  $roles['existing user'] = array(
    'name' => 'existing user',
    'weight' => 4,
  );

  // Exported role: former team members.
  $roles['former team members'] = array(
    'name' => 'former team members',
    'weight' => 5,
  );

  // Exported role: provisional team members.
  $roles['provisional team members'] = array(
    'name' => 'provisional team members',
    'weight' => 6,
  );

  // Exported role: security team.
  $roles['security team'] = array(
    'name' => 'security team',
    'weight' => 7,
  );

  // Exported role: site maintainer.
  $roles['site maintainer'] = array(
    'name' => 'site maintainer',
    'weight' => 8,
  );

  return $roles;
}
