<?php

/**
 * @file
 * sdo_content_type_issue.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function sdo_content_type_issue_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_anonymous_project_issue';
  $strongarm->value = 0;
  $export['comment_anonymous_project_issue'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_controls_project_issue';
  $strongarm->value = '3';
  $export['comment_controls_project_issue'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_mode_project_issue';
  $strongarm->value = 0;
  $export['comment_default_mode_project_issue'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_order_project_issue';
  $strongarm->value = '2';
  $export['comment_default_order_project_issue'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_per_page_project_issue';
  $strongarm->value = 600;
  $export['comment_default_per_page_project_issue'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_form_location_project_issue';
  $strongarm->value = 0;
  $export['comment_form_location_project_issue'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_preview_project_issue';
  $strongarm->value = '1';
  $export['comment_preview_project_issue'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_project_issue';
  $strongarm->value = '2';
  $export['comment_project_issue'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_subject_field_project_issue';
  $strongarm->value = 0;
  $export['comment_subject_field_project_issue'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'diff_enable_revisions_page_node_project_issue';
  $strongarm->value = 1;
  $export['diff_enable_revisions_page_node_project_issue'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'diff_show_preview_changes_node_project_issue';
  $strongarm->value = 1;
  $export['diff_show_preview_changes_node_project_issue'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'diff_view_mode_preview_node_project_issue';
  $strongarm->value = 'full';
  $export['diff_view_mode_preview_node_project_issue'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_node__project_issue';
  $strongarm->value = array(
    'view_modes' => array(
      'teaser' => array(
        'custom_settings' => TRUE,
      ),
      'nodechanges' => array(
        'custom_settings' => TRUE,
      ),
      'issuemetadata' => array(
        'custom_settings' => TRUE,
      ),
      'full' => array(
        'custom_settings' => FALSE,
      ),
      'rss' => array(
        'custom_settings' => FALSE,
      ),
      'search_index' => array(
        'custom_settings' => FALSE,
      ),
      'search_result' => array(
        'custom_settings' => FALSE,
      ),
      'print' => array(
        'custom_settings' => FALSE,
      ),
      'token' => array(
        'custom_settings' => FALSE,
      ),
      'diff_standard' => array(
        'custom_settings' => FALSE,
      ),
    ),
    'extra_fields' => array(
      'form' => array(
        'title' => array(
          'weight' => '-5',
        ),
        'nodechanges_comment_body' => array(
          'weight' => '60',
        ),
        'nodechanges_comment' => array(
          'weight' => '5',
        ),
      ),
      'display' => array(
        'update_link' => array(
          'issuemetadata' => array(
            'weight' => '12',
            'visible' => FALSE,
          ),
          'default' => array(
            'weight' => '100',
            'visible' => FALSE,
          ),
          'teaser' => array(
            'weight' => '100',
            'visible' => FALSE,
          ),
          'nodechanges' => array(
            'weight' => '22',
            'visible' => FALSE,
          ),
          'diff_standard' => array(
            'weight' => '100',
            'visible' => FALSE,
          ),
        ),
        'follow_link' => array(
          'issuemetadata' => array(
            'weight' => '110',
            'visible' => TRUE,
          ),
          'default' => array(
            'weight' => '110',
            'visible' => FALSE,
          ),
          'teaser' => array(
            'weight' => '100',
            'visible' => FALSE,
          ),
          'nodechanges' => array(
            'weight' => '100',
            'visible' => FALSE,
          ),
          'diff_standard' => array(
            'weight' => '100',
            'visible' => FALSE,
          ),
        ),
        'jump_links' => array(
          'issuemetadata' => array(
            'weight' => '13',
            'visible' => TRUE,
          ),
          'default' => array(
            'weight' => '120',
            'visible' => FALSE,
          ),
          'teaser' => array(
            'weight' => '100',
            'visible' => FALSE,
          ),
          'nodechanges' => array(
            'weight' => '21',
            'visible' => FALSE,
          ),
          'diff_standard' => array(
            'weight' => '100',
            'visible' => FALSE,
          ),
        ),
        'project_issue_created_by' => array(
          'issuemetadata' => array(
            'weight' => '11',
            'visible' => TRUE,
          ),
          'nodechanges' => array(
            'weight' => '13',
            'visible' => TRUE,
          ),
          'default' => array(
            'weight' => '60',
            'visible' => TRUE,
          ),
        ),
        'project_issue_created' => array(
          'issuemetadata' => array(
            'weight' => '21',
            'visible' => FALSE,
          ),
          'nodechanges' => array(
            'weight' => '14',
            'visible' => TRUE,
          ),
          'default' => array(
            'weight' => '80',
            'visible' => TRUE,
          ),
        ),
        'project_issue_updated' => array(
          'issuemetadata' => array(
            'weight' => '22',
            'visible' => FALSE,
          ),
          'nodechanges' => array(
            'weight' => '15',
            'visible' => TRUE,
          ),
          'default' => array(
            'weight' => '90',
            'visible' => TRUE,
          ),
        ),
      ),
    ),
  );
  $export['field_bundle_settings_node__project_issue'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_project_issue';
  $strongarm->value = array(
    0 => 'navigation',
  );
  $export['menu_options_project_issue'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_project_issue';
  $strongarm->value = 'navigation:0';
  $export['menu_parent_project_issue'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_project_issue';
  $strongarm->value = array(
    0 => 'status',
    1 => 'revision',
  );
  $export['node_options_project_issue'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_project_issue';
  $strongarm->value = '1';
  $export['node_preview_project_issue'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_project_issue';
  $strongarm->value = 1;
  $export['node_submitted_project_issue'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'project_issue_open_states';
  $strongarm->value = array(
    1 => '1',
    13 => '13',
    8 => '8',
    14 => '14',
    4 => '4',
    18 => '18',
    24 => '24',
    33 => '33',
    20 => '20',
    21 => '21',
    22 => '22',
    28 => '28',
    2 => 0,
    3 => 0,
    5 => 0,
    6 => 0,
    7 => 0,
    38 => 0,
  );
  $export['project_issue_open_states'] = $strongarm;

  return $export;
}
