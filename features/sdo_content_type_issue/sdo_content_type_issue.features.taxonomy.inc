<?php

/**
 * @file
 * sdo_content_type_issue.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function sdo_content_type_issue_taxonomy_default_vocabularies() {
  return array(
    'tags' => array(
      'name' => 'Tags',
      'machine_name' => 'tags',
      'description' => 'Tags',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'vocabulary_2' => array(
      'name' => 'Privacy',
      'machine_name' => 'vocabulary_2',
      'description' => 'Resolved cases with a published security advisory and no other confidential information can be set to public. Otherwise, they have to be kept confidential.',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'vocabulary_3' => array(
      'name' => 'Projects',
      'machine_name' => 'vocabulary_3',
      'description' => '',
      'hierarchy' => 1,
      'module' => 'project',
      'weight' => 0,
    ),
    'vocabulary_4' => array(
      'name' => 'Project Issue Types',
      'machine_name' => 'vocabulary_4',
      'description' => 'Used by Project Issue Availability module',
      'hierarchy' => 1,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
