<?php

/**
 * @file
 * sdo_content_type_issue.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function sdo_content_type_issue_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_credit|node|project_issue|form';
  $field_group->group_name = 'group_credit';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'project_issue';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Credit',
    'weight' => '9',
    'children' => array(
      0 => 'field_fixed',
      1 => 'field_reporters',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-credit field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_credit|node|project_issue|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Credit');

  return $field_groups;
}
