<?php

/**
 * @file
 * sdo_content_type_advisory.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function sdo_content_type_advisory_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function sdo_content_type_advisory_node_info() {
  $items = array(
    'advisory' => array(
      'name' => t('Security advisory'),
      'base' => 'node_content',
      'description' => t('A node type for security advisories.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => t('You can create a security advisory for your project. Please see the <a href="https://www.drupal.org/security/contrib">recent public Security Advisories on drupal.org</a> for examples.'),
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
