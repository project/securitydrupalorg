<?php

/**
 * @file
 * sdo_content_type_advisory.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function sdo_content_type_advisory_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_header|node|advisory|default';
  $field_group->group_name = 'group_header';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'advisory';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Header',
    'weight' => '40',
    'children' => array(
      0 => 'field_advisory_id',
      1 => 'field_date',
      2 => 'field_exploitable',
      3 => 'field_risk',
      4 => 'field_version',
      5 => 'field_versions_affected',
      6 => 'field_vulnerability',
    ),
    'format_type' => 'html-element',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'element' => 'div',
        'show_label' => 0,
        'label_element' => 'div',
        'classes' => '',
        'attributes' => '',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_header|node|advisory|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_header|node|advisory|form';
  $field_group->group_name = 'group_header';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'advisory';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Header',
    'weight' => '2',
    'children' => array(
      0 => 'field_advisory_id',
      1 => 'field_date',
      2 => 'field_exploitable',
      3 => 'field_risk',
      4 => 'field_version',
      5 => 'field_versions_affected',
      6 => 'field_vulnerability',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(),
    ),
  );
  $field_groups['group_header|node|advisory|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_header|node|advisory|teaser';
  $field_group->group_name = 'group_header';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'advisory';
  $field_group->mode = 'teaser';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Header',
    'weight' => '40',
    'children' => array(
      0 => 'field_advisory_id',
      1 => 'field_date',
      2 => 'field_exploitable',
      3 => 'field_project',
      4 => 'field_risk',
      5 => 'field_version',
      6 => 'field_vulnerability',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(),
    ),
  );
  $field_groups['group_header|node|advisory|teaser'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Header');

  return $field_groups;
}
