<?php print l(t('Create public security advisory'), 'https://www.drupal.org/node/add/sa', [
  'absolute' => TRUE,
  'query' => ['field_project' => $wrapper->field_sa_for->field_project->field_project_machine_name->value(['sanitize' => TRUE])],
]); ?>

<h4>Version</h4>
<?php print $wrapper->field_versions_affected->value->value(['sanitize' => TRUE]); ?>

<h4>Security risk</h4>
<pre><code><?php print securitydrupalorg_calc_risk(unserialize($wrapper->field_riskscore->value()), 'statement'); ?></code></pre>

<h4>Vulnerability</h4>
<pre><code><?php print implode(', ', $wrapper->field_vulnerability->value(['sanitize' => TRUE])); ?></code></pre>

<h4>Description</h4>
<pre><code><?php print check_plain($wrapper->field_description->value->value(['sanitize' => TRUE])); ?></code></pre>

<h4>Solution</h4>
<pre><code><?php print check_plain($wrapper->field_solution->value->value(['sanitize' => TRUE])); ?></code></pre>

<h4>Reported by user names</h4>
<pre><code><?php print check_plain(implode(', ', $wrapper->field_sa_for->field_reporters->value())); ?></code></pre>

<h4>Fixed by user names</h4>
<pre><code><?php print check_plain(implode(', ', $wrapper->field_sa_for->field_fixed->value())); ?></code></pre>
