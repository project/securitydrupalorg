<?php

// drush6 -v -r <path_to_d.o> scr kill.php <project_short_name>
$project = $_SERVER['argv'][8];

if (!isset($project)) {
  die("No project name");
}

foreach (project_release_query_releases(project_load($project)->nid) as $node) {
  drush_log(dt('Working on @nid', array('@nid' => $node->nid)));
  $node->status = NODE_NOT_PUBLISHED;
  node_save($node);
}
