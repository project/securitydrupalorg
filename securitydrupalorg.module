<?php

/**
 * @file
 * This module contains customizations used on security.drupal.org itself.
 */

/**
 * Implements hook_views_api().
 */
function securitydrupalorg_views_api() {
  return array(
    'api' => 2.0,
  );
}

/**
 * Implements hook_menu().
 */
function securitydrupalorg_menu() {
  $items = array();

  $items['admin/config/securitydrupalorg'] = array(
    'title' => 'Drupal.org sync settings',
    'access arguments' => array('administer site configuration'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('securitydrupalorg_sync_settings'),
  );
  $items['anonymous-issue-thank-you'] = array(
    'title' => 'Thank you for reporting',
    'access arguments' => array('access content'),
    'page callback' => 'securitydrupalorg_default_response_page',
  );
  $items['riskcalc'] = array(
    'title' => 'Security Risk Calculator',
    'access arguments' => array('access content'),
    'page callback' => 'drupal_goto',
    'page arguments' => ['https://www.drupal.org/riskcalc', ['external' => TRUE], '301'],
  );
  $items['team-members'] = [
    'title' => 'Team members redirect',
    'access arguments' => ['access content'],
    'page callback' => 'drupal_goto',
    'page arguments' => ['https://www.drupal.org/drupal-security-team/security-team-members', ['external' => TRUE], '301'],
  ];
  $items['dofeed'] = array(
    'title' => 'D.O feed',
    'page callback' => 'securitydrupalorg_dofeed',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Implements hook_menu_alter().
 */
function securitydrupalorg_menu_alter(&$menu) {
  if (isset($menu['taxonomy/term/%taxonomy_term'])) {
    $menu['taxonomy/term/%taxonomy_term']['access arguments'] = array('assign and be assigned project issues');
  }
}

/**
 * Implements hook_block_info().
 *
 * Blocks for securitydrupalorg module.
 */
function securitydrupalorg_block_info() {

  $blocks['sdo_project_versions'] = array(
    'info' => t('Project supported versions'),
    'cache' => DRUPAL_CACHE_PER_ROLE,
  );

  return $blocks;
}

/**
 * Implements hook_block_view().
 *
 * Blocks for securitydrupalorg module.
 */
function securitydrupalorg_block_view($delta = '') {

  switch ($delta) {
    case 'sdo_project_versions':

      $blocks['subject'] = t('Project versions');
      $blocks['content'] = securitydrupalorg_get_version_data();
      break;
  }

  return $blocks;
}

/**
 * Get the contents of "Project versions" block.
 *
 * @return string
 *   of version data
 */
function securitydrupalorg_get_version_data() {

  $project_name = '';
  $versions = array();

  $object = menu_get_object();
  if (!$object || !isset($object->type) || $object->type != 'project_issue') {
    return FALSE;
  }

  $node = node_load($object->nid);
  if ($node
      && isset($node->field_project)
      && !empty($node->field_project[LANGUAGE_NONE])) {

    $project = node_load($node->field_project[LANGUAGE_NONE][0]['target_id']);
    if ($project
        && isset($project->field_project_machine_name)
        && !empty($project->field_project_machine_name[LANGUAGE_NONE])) {
      $project_name = $project->field_project_machine_name[LANGUAGE_NONE][0]['value'];
    }
  }

  if (!$project_name
      || securitydrupalorg_get_releases($project_name, 6, $versions) === FALSE
      || securitydrupalorg_get_releases($project_name, 7, $versions) === FALSE
      || securitydrupalorg_get_releases($project_name, 8, $versions) === FALSE) {

    return '<div style="padding: 0.25em 0.5em; background-color: #fdd;">' . t('Error fetching Drupal API') . '</div>';
  }

  if (!$versions) {
    $output = '<div style="padding: 0.25em 0.5em; background-color: #fdd;">' . t('No supported versions') . '</div>';
  }
  else {
    $output = '';
    foreach ($versions as $drupal_version => $major_versions) {
      foreach ($major_versions as $major_version => $patch_level) {
        $output .= '<div style="padding: 0.25em 0.5em; background-color: ' . $patch_level['color'] . ';">' . $patch_level['version'] . '</div>';
      }
    }
  }

  return $output;
}

/**
 * Gets the release versions of a project for a major version.
 *
 * @param string $project
 *   The project you are looking up.
 * @param string $drupal_version
 *   Major version.
 * @param string $versions
 *   Minor version.
 *
 * @return bool
 *   Does not return
 */
function securitydrupalorg_get_releases($project, $drupal_version, &$versions) {
  $xml = @drupal_http_request("http://updates.drupal.org/release-history/$project/$drupal_version.x");
  if (!$xml || !($xml = simplexml_load_string($xml->data))) {
    return FALSE;
  }
  if (!isset($versions[$drupal_version])) {
    $versions[$drupal_version] = array();
  }

  foreach ($xml->releases as $release) {
    foreach ($release as $r) {
      if ((string) $r->status == 'published') {
        $version = (string) $r->version;
        $version_major = (string) $r->version_major;
        $version_patch = (string) $r->version_patch;

        if (!isset($versions[$drupal_version][$version_major])) {
          $versions[$drupal_version][$version_major] = array();
        }

        if (!isset($versions[$drupal_version][$version_major]['patch']) || $versions[$drupal_version][$version_major]['patch'] < $version_patch) {
          $versions[$drupal_version][$version_major]['patch'] = (string) $version_patch;
          $versions[$drupal_version][$version_major]['version'] = (string) $version;
        }

        // Check if the release is supported by the policy.
        if (is_numeric(str_replace($drupal_version . '.x-', '', $version))) {
          $versions[$drupal_version][$version_major]['color'] = '#dfd';
        }
        elseif (!isset($versions[$drupal_version][$version_major]['color'])) {
          $versions[$drupal_version][$version_major]['color'] = '#fdd';
        }
      }
    }
  }
}

/**
 * Provides a feed of issues for the drupalorg dashboard.
 */
function securitydrupalorg_dofeed() {
  drupal_add_http_header('Access-Control-Allow-Origin', 'https://www.drupal.org');
  drupal_add_http_header('Access-Control-Allow-Credentials', 'true');
  print views_embed_view('sdo_project_issue_user_issues', 'block_1');
}

/**
 * Solr settings for syncing projects over.
 */
function securitydrupalorg_sync_settings($form, &$form_state) {
  $form = array();
  $form['securitydrupalorg_project_sync_enable'] = array(
    '#type' => 'checkbox',
    '#title' => 'Enable project sync on cron',
    '#default_value' => variable_get('securitydrupalorg_project_sync_enable', TRUE),
  );
  $form['securitydrupalorg_tac_vid'] = array(
    '#type' => 'textfield',
    '#title' => 'tac vid',
    '#default_value' => variable_get('securitydrupalorg_tac_vid', 2),
  );
  $form['securitydrupalorg_tac_tid'] = array(
    '#type' => 'textfield',
    '#title' => 'tac tid',
    '#default_value' => variable_get('securitydrupalorg_tac_tid', 4),
  );
  $form['securitydrupalorg_solr_url'] = array(
    '#type' => 'textfield',
    '#title' => 'Solr url',
    '#default_value' => variable_get('securitydrupalorg_solr_url', 'http://localhost:8983/solr/select/'),
  );
  $form['securitydrupalorg_solr_rows'] = array(
    '#type' => 'textfield',
    '#title' => 'Solr rows per cron run',
    '#default_value' => variable_get('securitydrupalorg_solr_rows', '400'),
  );
  $form['securitydrupalorg_solr_last'] = array(
    '#type' => 'textfield',
    '#title' => 'Solr last sync time',
    '#default_value' => variable_get('securitydrupalorg_solr_last', '*'),
  );
  return system_settings_form($form);
}

/**
 * Implements hook_form_alter().
 */
function securitydrupalorg_form_alter(&$form, $form_state, $form_id) {
  // Add some code to add mantainers in.
  if ($form_id == 'project_issue_node_form' && isset($form['#node']->nid)) {
    $form['#validate'][] = 'securitydrupalorg_project_validate';
    $form['secmaintadd'] = array(
      '#type' => 'checkbox',
      '#title' => 'Add/update project maintainers',
      '#weight' => '100',
      '#access' => user_access('Change Field ACL'),
    );
  }
  // Hide this for now.
  if ($form_id == 'project_issue_node_form') {
    $form['taxonomy_vocabulary_4']['#access'] = FALSE;
    $form['field_issue_category']['und']['#default_value'] = 5;
    $form['#validate'][] = 'securitydrupalorg_validate_priority_change';
    $form['#submit'][] = 'securitydrupalorg_update_credit';
  }
  if ($form_id == 'project_issue_node_form'  && !isset($form['#node']->nid)) {
    $form['field_access']['#access'] = FALSE;
    $form['field_issue_priority']['#access'] = FALSE;
  }
  // If the report is submitted by an anonymous user, redirect to the thank-you
  // page where the default message is displayed.
  if ($form_id == 'project_issue_node_form' && $form['type']['#value'] == 'project_issue' && $form['uid']['#value'] === 0) {
    $form['#redirect'] = 'anonymous-issue-thank-you';
  }
  $defaults = '';
  if ($form_id == 'advisory_node_form') {
    if (isset($form['#node']->field_riskscore[LANGUAGE_NONE][0]['value'])) {
      $defaults = $form['#node']->field_riskscore[LANGUAGE_NONE][0]['value'];
    }
    $riskform = securitydrupalorg_riskcalc_form(array(), $form_state, $defaults);
    unset($riskform['submit']);
    $form['risk'] = $riskform;
    // New score.
    $form['field_riskscore']['#type'] = 'hidden';
    // Old score.
    $form['field_risk']['#type'] = 'hidden';
    $form['#validate'][] = 'securitydrupalorg_advisory_node_form_validate';
    $form['#submit'][] = 'securitydrupalorg_update_priority';

  }
}

/**
 * Custom validate form for advisories.
 */
function securitydrupalorg_advisory_node_form_validate(&$form, &$form_state) {
  $risk = securitydrupalorg_calc_risk($form_state['values']['risk'], 'array');
  $risk = serialize($risk);
  form_set_value($form['field_riskscore'], array('und' => array(0 => array('value' => $risk))), $form_state);

}

/**
 * Custom validate form for project issues.
 */
function securitydrupalorg_project_validate(&$form, &$form_state) {
  if ($form_state['values']['secmaintadd'] == 1) {
    securitydrupalorg_add_maint($form_state);
  }
}

/**
 * Function to edit form_state to add project owners in.
 */
function securitydrupalorg_add_maint(&$form_state) {
  // First off, we need to get an array of people who already have access.
  $uids = array();
  $count = 0;
  foreach ($form_state['values']['field_access']['und'] as $uid) {
    $count++;
    if (is_numeric($uid['uid'])) {
      $uids[] = $uid['uid'];
    }
  }

  // Now we call a json request to get the project maint from drupal.org.
  $project = node_load($form_state['node']->field_project['und'][0]['target_id'])->field_project_machine_name['und'][0]['value'];
  $maint = securitydrupalorg_get_maint($project);
  $newusers = array_diff($maint, $uids);
  foreach ($newusers as $newuser) {
    $count++;
    $form_state['values']['field_access']['und'][$count]['uid'] = $newuser;
  }
}

/**
 * Pulls a list of maintainers from drupal.org.
 *
 * @Param string $project_short_name
 *   the name of the project to sync
 */
function securitydrupalorg_get_maint($project_short_name) {
  $maints = array();
  if ($project_short_name == 'drupal') {
    $coreusers = variable_get('securitydrupalorg_drupal_json_override');
    $newusers = preg_split('/,/', $coreusers);
    foreach ($newusers as $username) {
      $testuser = user_load_by_name($username);
      $maints[] = $testuser->uid;
    }
  }
  else {
    $url = "https://www.drupal.org/project/" . check_plain($project_short_name) . "/maintainers.json";
    $file = drupal_http_request($url)->data;
    $drupalmaintarray = json_decode($file);
    foreach ($drupalmaintarray as $name) {
      $username = $name->name;
      $perms = (array) $name->permissions;
      if ($perms['write to vcs'] == 1) {
        $testuser = user_load_by_name($username);
        if (!isset($testuser->uid)) {
          $buid = bakery_request_account($username);
          if ($buid) {
            $maints[] = $buid;
          }
        }
        else {
          $maints[] = $testuser->uid;
        }
      }
    }
  }
  return $maints;
}

/**
 * Implements hook_cron().
 */
function securitydrupalorg_cron() {
  if (variable_get('securitydrupalorg_project_sync_enable', TRUE)) {
    securitydrupalorg_sync_projects();
  }
}

/**
 * Determine User UID of the project maintainer using bakery.
 *
 * Failback to uid 1 if Bakery doesn't exist but that really shouldn't happen.
 *
 * @param string $name
 *   User name.
 *
 * @return int
 *   User UID.
 */
function securitydrupalorg_sync_create_user($name) {
  $existing_account = user_load_by_name($name);
  if ($existing_account) {
    return $existing_account->uid;
  }
  elseif (function_exists('bakery_request_account')) {
    return bakery_request_account($name);
  }
  // This shouldn't happen.
  return FALSE;
}

/**
 * Sync projects from Drupal.org.
 *
 *  How does this work:
 *  Solr index is used to determine all projects. We always ask for 200 projects
 *  that changed since time X where X is a changed time of a last project that
 *  was synced before.
 *  Nodes are created and maintainers saved to field_project_maintainers as
 *  userreference.
 *
 * Potential problems: This will fail if more than securitydrupalorg_solr_rows
 * projects will be saved on drupal.org in the same second. Since
 * securitydrupalorg_solr_rows is >= 200, we assume this won't happen.
 */
function securitydrupalorg_sync_projects() {
  module_load_include('inc', 'node', 'node.pages');

  // Use Issue administrator user as a default owner of projects.
  $issue_admin = user_load_by_name('Issue administrator');
  $node_owner = $issue_admin->uid;
  if (!$node_owner) {
    $node_owner = 1;
  }

  $solr_url = variable_get('securitydrupalorg_solr_url', 'http://localhost:8983/solr/select/');
  $rows = variable_get('securitydrupalorg_solr_rows', '600');
  $last = variable_get('securitydrupalorg_solr_last', '*');
  $query = "?fq=bundle:(project_module%20OR%20project_theme%20OR%20project_distribution%20OR%20project_general)&hash:sh0zn1&fq=-is_uid:0&fq=bs_status:1&fq=-sm_field_project_type:sandbox&fl=is_uid,ds_changed,ss_name,entity_id,url,path_alias,label,ss_field_project_machine_name,content&fq=ds_changed:[" . $last . "%20TO%20*]&wt=json&rows=" . $rows . "&sort=ds_changed%20asc";
  $request_url = $solr_url . $query;
  $result = drupal_http_request($request_url);
  if ($result->code == 200) {
    $data = json_decode($result->data, TRUE);
    foreach ($data['response']['docs'] as $project) {
      $short_name = $project['ss_field_project_machine_name'];
      // Does this project exist?
      $project_nid = db_query("SELECT entity_id FROM {field_data_field_project_machine_name} WHERE field_project_machine_name_value = :shortname", array(':shortname' => $short_name))->fetchField();
      if ($project_nid) {
        // Project exists.
        securitydrupalorg_update_project($project_nid, $project);
      }
      else {
        // Create new node.
        $node = new stdClass();
        $node->type = 'project';
        $node->uid = $node_owner;
        $node->status = 1;
        $node->title = $project['label'];
        $node->body = trim($project['content']);
        $node->format = 1;
        $node->field_project_type[LANGUAGE_NONE][0]['value'] = 'full';
        $node->field_project_machine_name[LANGUAGE_NONE][0]['value'] = $project['ss_field_project_machine_name'];
        $owner_name = $project['ss_name'];
        $owner_uid = securitydrupalorg_sync_create_user($owner_name);
        if (empty($owner_uid)) {
          watchdog('securitydrupalorg', 'Creating user on s.d.o failed, username @user', array('@user' => $owner_name), WATCHDOG_ERROR);
        }
        else {
          $node->field_project_maintainers[LANGUAGE_NONE][] = array('uid' => $owner_uid);
        }

        // Save node.
        node_save($node);
        if (empty($node->nid)) {
          $error = TRUE;
          watchdog('securitydrupalorg', 'node_save failed for @data', array('@data' => print_r($node, TRUE)), WATCHDOG_ERROR);
          break;
        }
      }
    }
    // Save the last one.
    if (!isset($error)) {
      variable_set('securitydrupalorg_solr_last', $project['ds_changed']);
    }
  }
  else {
    watchdog('securitydrupalorg', 'Solr request failed to URL: @url with code @code', array('@url' => $request_url, '@code' => $result->code), WATCHDOG_ERROR);
  }
}

/**
 * Check if existing project changed and save it.
 *
 * Metrics to use: Did title or maintainer change?
 *
 * @param int $project_nid
 *   Project node ID.
 * @param string $project
 *   Solr result set.
 */
function securitydrupalorg_update_project($project_nid, $project) {
  $node = node_load($project_nid);
  $owner_uid = securitydrupalorg_sync_create_user($project['ss_name']);
  if ($node->title != $project['label']) {
    // Node changed, update it.
    $node->title = $project['label'];
    // We want to clear everyone who might be in here.
    unset($node->field_project_maintainers);
    $node->field_project_maintainers[LANGUAGE_NONE][0]['uid'] = $owner_uid;
    node_save($node);
  }
}

/**
 * Sync a project from www.drupal.org.
 */
function securitydrupalorg_sync_project($project_short_name) {
  $project_data = json_decode(drupal_http_request(url('https://www.drupal.org/api-d7/node.json', ['query' => ['field_project_machine_name' => $project_short_name]]))->data)->list[0];
  $author_data = json_decode(drupal_http_request($project_data->author->uri, ['headers' => ['Accept' => 'application/json']])->data);

  // Use Issue administrator user as a default owner of projects.
  if (!($node_owner = user_load_by_name('Issue administrator')->uid)) {
    $node_owner = 1;
  }

  if ($node = project_load($project_data->field_project_machine_name)) {
    // Todo update projecct.
  }
  else {
    // Create new node.
    $node = new stdClass();
    $node->type = 'project';
    $node->uid = $node_owner;
    $node->status = 1;
    $node->title = $project_data->title;
    $node->body = trim($project_data->body->value);
    $node->format = filter_default_format();
    $node->field_project_type[LANGUAGE_NONE][0]['value'] = 'full';
    $node->field_project_machine_name[LANGUAGE_NONE][0]['value'] = $project_data->field_project_machine_name;
    if ($owner_uid = securitydrupalorg_sync_create_user($author_data->name)) {
      $node->field_project_maintainers[LANGUAGE_NONE][] = array('uid' => $owner_uid);
    }
    else {
      watchdog('securitydrupalorg', 'Creating user on s.d.o failed, username @user', array('@user' => $author_data->name), WATCHDOG_ERROR);
    }

    // Save node.
    node_save($node);
    if (empty($node->nid)) {
      $error = TRUE;
      watchdog('securitydrupalorg', 'node_save failed for @data', array('@data' => print_r($node, TRUE)), WATCHDOG_ERROR);
    }
  }
}

/**
 * Admin interface for entering the default response.
 */
function securitydrupalorg_default_response_settings() {
  $form['securitydrupalorg_default_response_settings'] = array(
    '#type' => 'textarea',
    '#title' => 'Default response to reports',
    '#default_value' => variable_get('securitydrupalorg_default_response_settings', ''),
  );
  return system_settings_form($form);
}

/**
 * Callback function for returning the default response message.
 */
function securitydrupalorg_default_response_page() {
  $message = variable_get('securitydrupalorg_default_response_settings', 'Thank you for reporting a potential security issue. One of the Drupal Security Team members will get back to you once we can confirm the issue exists.');
  return filter_xss($message);
}

/**
 * Implements hook_node_insert().
 */
function securitydrupalorg_node_insert($node) {
  securitydrupalorg_node_acl_do($node);
}

/**
 * Implements hook_node_update().
 */
function securitydrupalorg_node_update($node) {
  securitydrupalorg_node_acl_do($node);
}

/**
 * S.D.O SA, update to add issue ACL to SA nodes.
 */
function securitydrupalorg_node_acl_do($node) {
  if ($node->type == 'advisory') {
    // Make sure the node is not on any acl.
    acl_node_clear_acls($node->nid, 'field_acl');
    $issueid = $node->field_sa_for['und'][0]['target_id'];
    $acl_id = acl_get_id_by_name('field_acl', 'update_' . $issueid);
    if ($acl_id) {
      acl_node_add_acl($node->nid, $acl_id, 1, 1, 0);
    }
  }
}

/**
 * Subscribe everyone to all projects.
 */
function securitydrupalorg_user_insert(&$edit, $account, $category) {
  $account->project_issue_notification = array(
    'notify_own_updates' => 1,
    'level' => 2,
    'mail_body' => 1,
    'mail_subject_project' => 1,
    'mail_subject_category' => 1,
  );
  project_issue_notification_user_settings_save($account);
}

/**
 * Form to display questions for risk level calculations.
 *
 * Can take an array of defaults passed from the node edit form.
 */
function securitydrupalorg_riskcalc_form($form, &$form_state, $defaults = NULL) {
  if (isset($defaults)) {
    $defaults = unserialize($defaults);
  }
  $form['help'] = array(
    '#markup' => "The questions below will help calculate the risk level for a security issue",
  );
  $form['risk'] = array(
    '#type' => 'fieldset',
    '#tree' => TRUE,
    '#title' => 'Risk Calculator',
  );
  $acoptions = array(
    'None' => t('None (user visits page)'),
    'Basic' => t("Basic or routine (user must follow specific path)"),
    'Complex' => t("Complex or highly specific (multi-step, unintuitive process with high number of dependencies)"),
  );
  $form['risk']['AC'] = array(
    '#title' => t("Access complexity: how difficult is it for the attacker to leverage the vulnerability?"),
    '#type' => 'select',
    '#required' => TRUE,
    '#options' => $acoptions,
    '#default_value' => $defaults['AC'],
  );
  $aoptions = array(
    'None' => t("None (all/anonymous users)"),
    'User' => t("User-level access (basic/commonly assigned permissions)"),
    'Admin' => "Administrator (broad permissions required where 'restrict access' is set to false)",
  );
  $form['risk']['A'] = array(
    '#title' => t("Authentication: what privilege level is required for an exploit to be successful?"),
    '#type' => 'select',
    '#required' => TRUE,
    '#options' => $aoptions,
    '#default_value' => $defaults['A'],

  );
  $cioptions = array(
    'All' => t("All non-public data is accessible"),
    'Some' => t("Certain non-public data is released"),
    'None' => t("No confidentiality impact"),
  );
  $form['risk']['CI'] = array(
    '#title' => t("Confidentiality impact: does this vulnerability cause non-public data to be accessible?"),
    '#type' => 'select',
    '#required' => TRUE,
    '#options' => $cioptions,
    '#default_value' => $defaults['CI'],

  );
  $iioptions = array(
    'All' => t("All data can be modified or deleted"),
    'Some' => t("Some data can be modified"),
    'None' => t("Data integrity remains intact"),
  );
  $form['risk']['II'] = array(
    '#title' => t("Integrity impact: can this exploit allow system data (or data handled by the system) to be compromised?"),
    '#type' => 'select',
    '#required' => TRUE,
    '#options' => $iioptions,
    '#default_value' => $defaults['II'],

  );
  $eoptions = array(
    'Exploit' => t("Exploit exists (documented or deployed exploit code already in the wild)"),
    'Proof' => t("Proof of concept exists (documented methods for developing exploit exist in the wild)"),
    'Theoretical' => t("Theoretical or white-hat (no public exploit code or documentation on development exists)"),
  );
  $form['risk']['E'] = array(
    '#title' => t("Zero-day impact: does a known exploit exist?"),
    '#type' => 'select',
    '#required' => TRUE,
    '#options' => $eoptions,
    '#default_value' => $defaults['E'],

  );
  $tdoptions = array(
    'All' => t("All module configurations are exploitable"),
    'Default' => t("Default or common module configurations are exploitable, but a config change can disable the exploit"),
    'Uncommon' => t("Only uncommon module configurations are exploitable"),
  );

  $form['risk']['TD'] = array(
    '#title' => t("Target distribution: what percentage of module users are affected?"),
    '#type' => 'select',
    '#required' => TRUE,
    '#options' => $tdoptions,
    '#default_value' => $defaults['TD'],

  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'submit',
  );
  return $form;
}

/**
 * Form submit hook to display risk levels mostly for testing.
 */
function securitydrupalorg_riskcalc_form_submit($form, &$form_state) {
  $risk = $form_state['values']['risk'];
  $statement = securitydrupalorg_calc_risk($risk);
  drupal_set_message($statement);

}

/**
 * Function to calculate risk.
 */
function securitydrupalorg_calc_risk($risk, $return = 'text') {
  // For each question setup a value, long form so it is quicker to read.
  $score = 0;
  $statement = '';

  // Access complexity.
  if ($risk['AC'] == 'None') {
    $statement .= "AC:None"; $score = $score + 4;
  }
  if ($risk['AC'] == 'Basic') {
    $statement .= "AC:Basic"; $score = $score + 2;
  }
  if ($risk['AC'] == 'Complex') {
    $statement .= "AC:Complex"; $score = $score + 1;
  }
  // Authentication.
  if ($risk['A'] == 'None') {
    $statement .= "/A:None"; $score = $score + 4;
  }
  if ($risk['A'] == 'User') {
    $statement .= "/A:User"; $score = $score + 2;
  }
  if ($risk['A'] == 'Admin') {
    $statement .= "/A:Admin"; $score = $score + 1;
  }
  // Confidentiality impact.
  if ($risk['CI'] == 'All') {
    $statement .= "/CI:All"; $score = $score + 5;
  }
  if ($risk['CI'] == 'Some') {
    $statement .= "/CI:Some"; $score = $score + 3;
  }
  if ($risk['CI'] == 'None') {
    $statement .= "/CI:None"; $score = $score + 0;
  }
  // Integrity impact.
  if ($risk['II'] == 'All') {
    $statement .= "/II:All"; $score = $score + 5;
  }
  if ($risk['II'] == 'Some') {
    $statement .= "/II:Some"; $score = $score + 3;
  }
  if ($risk['II'] == 'None') {
    $statement .= "/II:None"; $score = $score + 0;
  }
  // Exploitability.
  if ($risk['E'] == 'Exploit') {
    $statement .= "/E:Exploit"; $score = $score + 4;
  }
  if ($risk['E'] == 'Proof') {
    $statement .= "/E:Proof"; $score = $score + 2;
  }
  if ($risk['E'] == 'Theoretical') {
    $statement .= "/E:Theoretical"; $score = $score + 1;
  }
  // Target distribution.
  if ($risk['TD'] == 'All') {
    $statement .= "/TD:All"; $score = $score + 3;
  }
  if ($risk['TD'] == 'Default') {
    $statement .= "/TD:Default"; $score = $score + 2;
  }
  if ($risk['TD'] == 'Uncommon') {
    $statement .= "/TD:Uncommon"; $score = $score + 1;
  }
  if ($score > 0 && $score <= 4) {
    $text = '<span style="padding:1px;display:inline-block;background-color:#01A46D;color:black"> Not Critical</span>';
  }
  if ($score >= 5 && $score <= 9) {
    $text = '<span style="padding:1px;display:inline-block;background-color:#377FC7;color:black"> Less Critical</span>';
  }
  if ($score >= 10 && $score <= 14) {
    $text = '<span style="padding:1px;display:inline-block;background-color:#F5D800;color:black"> Moderately Critical</span>';
  }
  if ($score >= 15 && $score <= 19) {
    $text = '<span style="padding:1px;display:inline-block;background-color:#FF9B2B;color:black"> Critical</span>';
  }
  if ($score >= 20 && $score <= 28) {
    $text = '<span style="padding:1px;display:inline-block;background-color:#EC3E40;color:white"> Highly Critical</span>';
  }

  switch ($return) {
    case 'text':
      return $score . "/25 (" . $text . ") " . $statement;

    case 'statement':
      return $statement;

    case 'array':
      return $risk;
  }
}

/**
 * Implements hook_file_mimetype_mapping_alter().
 */
function securitydrupalorg_file_mimetype_mapping_alter(&$mapping) {
  // Handle .patch and .diff files like regular plain text files.
  $mapping['extensions']['diff'] = 292;
  $mapping['extensions']['patch'] = 292;
}

/**
 * On form submit update critical score when the sa node is saved.
 */
function securitydrupalorg_update_priority(&$form, &$form_state) {
  $risk = securitydrupalorg_calc_risk($form_state['values']['risk'], 'text');
  // We should build an API around the risk calc.
  $risk = strip_tags($risk);
  preg_match('/\(([^\)]+)\)/', $risk, $risk);
  $risk = trim($risk[1]);
  $fip = field_info_field('field_issue_priority');
  $flipvalues = array_flip(list_allowed_values($fip));
  $risk = $flipvalues[$risk];
  $issuenode = node_load($form_state['values']['field_sa_for'][LANGUAGE_NONE][0]['target_id']);
  $issuenode->field_issue_priority[LANGUAGE_NONE][0]['value'] = $risk;
  $issuenode->nodechanges_skip = TRUE;
  node_save($issuenode);
}

/**
 * On form submit update credits score when the sa node is saved.
 */
function securitydrupalorg_update_credit(&$form, &$form_state) {
  $sanode = securitydrupalorg_find_sa_by_issuenid($form_state['node']->nid);
  if (is_numeric($sanode)) {
    $sanode = node_load($sanode);
    $fixed = securitydrupalorg_update_credit_format('field_fixed', $form_state);
    $reported = securitydrupalorg_update_credit_format('field_reporters', $form_state);
    $sanode->field_fixed_by[LANGUAGE_NONE][0]['value'] = $fixed;
    $sanode->field_reported_by[LANGUAGE_NONE][0]['value'] = $reported;
    $sanode->nodechanges_skip = TRUE;
    $sanode->revision = TRUE;
    node_save($sanode);
  }
}

/**
 * Formats an array of usernames for SA updates
 */
function securitydrupalorg_update_credit_format($field, $form_state) {
  if (is_array($form_state['values'][$field]['und'])) {
    $data = '<ul>';
    foreach ($form_state['values'][$field]['und'] as $person) {
      $account = user_load_by_name($person['value']);
      $do_uid =  drupalorg_crosssite_get_drupalorg_uid($account);
      $fullname = $account->field_crosssite_first_name[LANGUAGE_NONE][0]['value'] . ' ' . $account->field_crosssite_last_name[LANGUAGE_NONE][0]['value'];
      if (strlen($fullname) < 3) {
        $fullname = $account->name;
      }
      if (isset($account->roles[5])) {
        $data .= "<li><a href='https://www.drupal.org/user/". $do_uid ."'>" .  $fullname . "</a> of the Drupal Security Team</li>\n";
      }
      else {
        $data .= "<li><a href='https://www.drupal.org/user/" . $do_uid . "'>" . $fullname . "</a></li>\n";
      }
    }
  }
  $data .="</ul>";
  return $data;
}

/**
 * On validate if someone is trying to change the issue priority, tell them how.
 */
function securitydrupalorg_validate_priority_change(&$form, &$form_state) {
  $submitstatus = $form_state['values']['field_issue_priority'][LANGUAGE_NONE][0]['value'];
  $nodestatus = $form['field_issue_priority']['und']['#default_value'][0];
  if ($submitstatus != $nodestatus) {
    // Someone is changing the risk level
    // does this node already have an SA.
    $advisory = securitydrupalorg_find_sa_by_issuenid($form['#node']->nid);
    if (is_numeric($advisory)) {
      $link = l(t("Please edit the current advisory"), 'node/' . $advisory . '/edit', array('attributes' => array('target' => '_blank')));
    }
    else {
      $link = l(t("Please create an advisory"), 'node/add/advisory', array('query' => array('field_sa_for' => $form['#node']->nid), 'attributes' => array('target' => '_blank')));
    }
    form_set_error('field_issue_priority', $link . ' to modify the risk level');
  }
}

/**
 * Given an issueID, find the advisory nid.
 */
function securitydrupalorg_find_sa_by_issuenid($nid) {
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'advisory')
    ->propertyCondition('status', NODE_PUBLISHED)
    ->fieldCondition('field_sa_for', 'target_id', $nid);
  $query->addMetaData('account', user_load(1));
  $result = $query->execute();
  if (isset($result['node'])) {
    // Get the last advisory.
    return end($result['node'])->nid;
  }
  else {
    return FALSE;
  }
}

/**
 * Get a list of usernames associated with an issue.
 */
function securitydrupalorg_comment_on_issue($form, $form_state, $entity_type, $entity) {
  static $people = [];

  if (empty($entity->nid)) {
    return [];
  }

  $key = $entity_type . ':' . $entity->nid;
  if (!isset($people[$key])) {
    // Commenters.
    $uids = db_query("SELECT DISTINCT c.uid FROM {comment} c WHERE c.nid = :nid", [
      ':nid' => $entity->nid,
    ])->fetchCol();

    // Author.
    $uids[] = $entity->uid;

    // Granted access.
    if (!empty($entity->field_access[LANGUAGE_NONE])) {
      $uids = array_merge($uids, array_column($entity->field_access[LANGUAGE_NONE], 'uid'));
    }

    // Fill out usernames.
    foreach (user_load_multiple($uids) as $account) {
      $name = check_plain($account->name);
      $people[$key][$name] = $name;
    }
  }

  return $people[$key];
}
