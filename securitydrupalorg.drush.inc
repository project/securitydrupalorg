<?php

/**
 * @file
 * Drush scripts.
 */

/**
 * Implements hook_drush_command().
 */
function securitydrupalorg_drush_command() {
  return [
    'sync-project' => [
      'description' => 'Sync a project from www.drupal.org',
      'arguments' => [
        'name' => 'Project short name',
      ],
    ],
  ];
}

/**
 * Sync a project from d.o.
 */
function drush_securitydrupalorg_sync_project($project_short_name) {
  securitydrupalorg_sync_project($project_short_name);
}
